# Project: Mystery Castle 

![setup][img01]

## Table of Contents 
- Overview
  - Terms
  - Design Goals
- Setup
  - Hardware
    - HP EliteBook
    - Raspberry Pi W/ WiFi and various Debian-based Linux devices
    - Configuring network interfaces
  - Client Node
    - Install required software
    - Identify Interfaces
    - blacklist wireless interfaces from NetworkManager
    - Set up interfaces
  - Uplink Node
    - Initial configuration
    - Configuring the wireless interface
    - Configure NAT
    - Configure DHCP (We are planning to use something other than IPv4 later, but this is the prototype)
  - Mesh Gateway 
    - Install required software
    - Identify Interfaces
    - blacklist wireless interfaces from NetworkManager
    - Set up interfaces
- Troubleshooting
  - Can't set up wireless on ibss mode
  - batctl neighbors not appearing
  - Addressing 
  - Taking forever to start (4 minutes to boot)


## Overview

Project Phoenix hopes to bring the concept of decentralized community-driven
networking to the general public similar to projects like NYCMesh and Guifi Net.
To better illustrate the ultimate goal of the project, Project Phoenix has 
developed a simple demonstration of mesh technologies leveraging easy to acquire
equipment running free and open-source software. The end goal is simply
establishing a clear and practical example of how mesh technologies work for 
anyone interested in the project. 

For the initial demo, we've acquired four HP Elitebook 840s and one Raspberry pi 3 B+.
The HP's run a free and open-source OS along with the Raspberry Pi. Networking
is handled by standard protocols except on Layer 2 of the OSI model. To optimize
the network, we're using the B.A.T.M.A.N routing protocol which specializes in 
multi-hop mobile ad-hoc networking. As of Kernel version 2.6.38, the batman-adv
kernel module has been included with the Linux kernel.

One laptop acts
both as a mesh-client and a mesh-gateway. The mesh-gateway is responsible for
bridging the wireless "mesh" network to a mesh-uplink. The mesh-uplink is
responsible for managing logical addressing on the Network-Layer. It also is set
up as a masquerade to forward traffic between mesh nodes and outside networks. 

B.A.T.M.A.N Advanced was chosen because it addresses key issues with wireless
mesh networking. B.A.T.M.A.N Advanced is also shipped with the Linux kernel as
of 2.x. 

![Diagram][dgm01]

### Terms
  - Mesh uplink - Node responsible for routing traffic to the Internet on behalf 
                  of mesh nodes.

  - Mesh Gateway - Node responsible for announcing route to Internet

  - Mesh Client - Node running BATMAN-ADV and connected to the demo network. 

  - Interface - Network device (eth0, wlp0s3, wlan0, etc...)

### Design Goals
Three foundational concepts are in mind when designing this demo.
  1) Portability
      - Transfer between venues should be simple. 
      - Internet Connectivity should be uncomplicated.
  2) Simplicity
      - The demo should not break if you breathe on it wrong. 
  3) Functionality
      - The concepts of a mesh network should be apparent, including demo
        software that illustrates mesh functionality while being exciting enough
        for general audiences. 

## Setup


### Hardware


#### HP EliteBook
Model: HP EliteBook 840 G2
OS: Ubuntu 18.04.1 LTS
Ethernet: I218-LM 
Wireless: Intel Wireless 3160 #83 

#### Raspberry Pi W/ WiFi
Model: Raspberry Pi 3 Model B+
OS: Raspbian <insert version> (minimal needs more testing)

### Client Node

A client node is simply a device on and using the network.  
As a client, one is able to connect to the other devices on the network.  
If there is an uplink node on the network, it should be able to connect to the rest
of the internet, too.

###### Install required software

You need the batctl package.
You also need to make sure your device is using a systemd service called "networking.service"

You install these packages by typing the following
```
$ sudo apt install batctl ifupdown
```
You can obtain either ifupdown of ifupdown2. 
These are just different versions, which you choose is up to you.

Eye rolling network admins:
Open-mesh.org had documentation for setting this up with the networking service in mind.
If you want to do it using NetworkManager.service or want us to change to it, let us know how.

###### Identify Interfaces

Identify interfaces for configuration by typing:

```
$ sudo ip link 
```
The output will look something like this


```
...<Truncated>

2: enp0s25: <NO-CARRIER, BROADCAST,MULTICAST,UP> mtu 1500...
  link/ether <MAC ADDRESS> brd <Broadcast address>

3: wlo1: <BROADCAST,MULTICAST,UP> mtu 1500...
  link/ether <MAC ADDRESS> brd <Broadcast address>
```

In this case, wlo1 and enp0s25 are the interfaces we are looking at on the device.
* enp0s25 is our Ethernet device interface
* wlo1 is our WiFi device interface
* Our's wasn't connected to the internet. If your's is, it may look slightly different

###### blacklist wireless interfaces from NetworkManager

Network Manager will take control of your devices, unless you blacklist it in the
Network Manager configuration file.
Network Manager knows the interface by MAC (Media Access Control) address.

Edit /etc/NetworkManager/NetworkManager.conf and add ("<>" needs to be replaces with the MAC address. 
Format of MAC addresses is 00:00:00:00:00:00) 
```
[keyfile]
unmanaged-devices=mac:< wlo1/enp0s25 link address >
```
(No spaces needed)

###### Set up interfaces

As previously detailed, our WiFi interface is wlo1.  
This interface needs to be prepared for use by B.A.T.M.A.N.

Edit the /etc/network/interfaces file and add the following:  
```
auto wlo1
iface wlo1 inet manual
  mtu 1532
  wireless-mode ad-hoc
  wireless-essid mesh
  wireless-freq 2412
  # "wireless-channel 1" should do the same hing as freq
```

B.A.T.M.A.N uses the bat0 interface. wlo1 is **not** used for logical routing. 
As a result, we add our bat0 virtual interface by adding this to the same /etc/network/interfaces file.

```
auto bat0
iface bat0 inet dhcp
  pre-up batctl if add wlo1
  post-up batctl gw_mode client
  pre-down batctl if del wlo1
```

##### For those of you wondering what the flip you just typed in:  
**auto \*interface\*** makes the device setup automatic.  
**iface** details the device that will be modified.  
**inet** (or inet6) is the internet version, meaning (most of the time) IPv4/6.   
**dhcp/manual/static** is the chosen method of obtaining an IP or device activation.  
**mtu** (maximum transmission unit) is increased by 32 bits to fit B.A.T.M.A.N. information.  
**wireless-mode** needs to be ad-hoc, as that is the method B.A.T.M.A.N. uses.  
**wireless-essid** is always followed by the chosen network name.  
**wireless-freq/wireless-channel** is the hertz/channel the network is communicating on.  
**pre-up** says to do something (such as a command) before bringing up the interface.  
**post-up** is after bringing up the interface.  
**pre-down** is before taking down the interface (when it does).  
**batctl** is the command for controlling B.A.T.M.A.N.  
**if** is batctl's argument for affecting interfaces.  
**add/del** for what is happening to the chosen interface on (in this case) bat0.  
**wlo1** is the chosen device for B.A.T.M.A.N. to use.  
**gw_mode** is a B.A.T.M.A.N. argument for setting if the device will be at server or client.  
**client** is the chosen gw_mode.  

### Uplink Node
More often than not the demo will be presented at a location with wireless  
networking. Be sure to acquire the configuration details of the wireless network

* Does the network use a captive portal to log in? 
  * If so, you need to open the browser to get on the internet
  * if you are using minimal, you need a method to pass the captive portal
* Does the pi support the WiFi version?
  * If the pi comes with the WiFi card, it is probably fine
  * If you need a WiFi card, the version will be written on the box
  * Common ones today are version b, g, n, and ac. (recently released is ax)

###### Initial configuration

For this demo, Raspbian was chosen to run on the Raspberry pi. After installing
the OS, run: 
```
# raspi-config
```
- Connect to a wireless network to patch the system. 

The following third party software is required:
  - dnsmasq
  - iptables
  - wireless-tools

###### Configure NAT
To enable IP packet forwarding, edit /etc/sysctl.conf. 

  - Uncomment net.ipv4.ip_forward=1

The following commands configure NAT using iptables. 
```
$ sudo iptables -A POSTROUTING \
-o wlan0 \
-j MASQUERADE 
-t nat

$ sudo iptables -A FORWARD \
-i wlan0 \
-o eth0 \
-m state \
--state RELATED,ESTABLISHED \
-j ACCEPT 

$ sudo iptables -A FORWARD \
-i eth0 \
-o wlan0 \
-j ACCEPT \

$ sudo iptables-save > /etc/iptables.rules
```

###### Configuring the wireless interface
This configuration enables roaming between different SSIDs.

Edit /etc/wpa_suppliciant/wpa_suppliciant.conf

For each desired wireless network, add the following;
```
network={       
  ssid="foonet"
  psk="barpass"  
  id_str="wlan1" 
  priority=1
}
```

###### Configuring network interfaces

edit /etc/network/interfaces


```
# Configure wireless network
allow-hotplug wlan0
iface wlan0 inet manual
  wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf

iface wlan1 inet dhcp

# Configure eth0
auto eth0
iface eth0 inet static
  address 10.27.0.1
  netmask 255.255.255.0
  post-up iptables-restore < /etc/iptables.rules
```

For the mesh network, ip addressing is currently handled by the uplink node.
In the future IP addressing will be handled by another means. 

The 10.27.0.0/24 subnet is allocated for the mesh IBSS. The IBSS is configured
and managed by the mesh-gateway. 

### Mesh Gateway

###### Install required software

```
$ sudo apt-get install batctl bridge-utils
```

###### Identify Interfaces

Identify interfaces for configuration
```
$ sudo ip link 
...<Truncated>

2: enp0s25: <NO-CARRIER, BROADCAST,MULTICAST,UP> mtu 1500...
  link/ether <MAC ADDRESS> brd <Broadcast address>

3: wlo1: <BROADCAST,MULTICAST,UP> mtu 1500...
  link/ether <MAC ADDRESS> brd <Broadcast address>

...<Truncated>
```

###### blacklist wireless interfaces from NetworkManager

Edit /etc/NetworkManager/NetworkManager.conf
```
[keyfile]
unmanaged-devices=mac:< wlo1 link/ether address >
```

After reloading the network, NetworkManager will no-longer manage the wireless interface
interface. Without this configuration, NM will over-rule any configurations you
set manually in /etc/network/interfaces. 

###### Set up interfaces

Configure the LAN access port as shown: 
```
auto enp0s25
iface enp0s25 inet manual
```

For the wireless interface, we'll be using an easy to remember alias. 
```
auto wlan0
iface wlan0 inet manual
  pre-up iw phy phy0 interface add wlan0 type ibss
  wireless-essid mesh
  wireless-mode ad-hoc
  wireless-freq 2412
  # We add 32 additional bits to fit the B.A.T.M.A.N datagram headers.
  mtu 1532
```

B.A.T.M.A.N uses the bat0 interface for TX/RX. wlan0 is **not** used for logical
routing. 
```
auto bat0
iface bat0 inet manual
  pre-up batctl if add wlan0
  post-up batctl gw_mode server
  pre-down batctl if del wlan0
```

Finally we configure a bridge between the LAN and WAN adapters.
```
auto br0
iface br0 inet dhcp
  bridge_ports enp0s25 bat0
```

For now, the prototype uses a contemporary logical addressing service. DHCP
works, but the next version of this prototype will use a decentralized
addressing scheme like CJDNS.


### Troubleshooting
Incoming...

[dgm01]: img/mc-diagram.png
[img01]: img/Scaled.png
