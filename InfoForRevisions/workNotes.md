# for the Latitude D830

iw is used to delete he wireless interface, instead of ip link.  
```iw dev wlan0 del```

Command for startin gateway.  
```batctl gw_mode server```

aquire ssid interface name scan  
```wpa_passphrase "Dr Bobs" 12345678 ```

piped into the wpa-supplicant.conf
This is needed because normally wpa-supplicant converts the key.
for some reason, our device did not.

So the full command is

```wpa_passphrase "Dr Bobs" 12345678 >> /etc/wpa_supplicant/wpa_supplicant.conf ```



```wpa_supplicant -B -i wlanDevice -c /etc/wpa_supplicant/wpa_supplicant.conf```

get an IP
```dhclient wlanDevice```

enable systemd-resolvd and set config for obtaining DNS

```systemctl start systemd-resolved```

```vim /etc/resolve.conf```

In the file:  
```nameserver 1.1.1.1```

allow for forwarding on the device
```sudo echo 1 > /proc/sys/net/ipv4/ip_forward ```

*Look at IPTABLES rules file for the iptables things.
This is what happens, now*


```batctl if add bat0```

add a route
```ip route add local 10.27.0.0/24 dev bat0```

Order seemed to matter.
For the general sections of these, do them backwards... For when moving backwards still is possible.

Other?:
pings were getting dropped it was a route problem.  
Look at "add a route".

later arps were being sent, and pings were getting noticed, but didn't return.
Possible firewall problem.


