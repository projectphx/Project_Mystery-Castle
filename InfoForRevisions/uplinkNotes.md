# What is being done
Creating a DHCP server for the Uplink

### Extra Info
resolvd might conflict with the dhcp server
when systemd is above 2.32, this works


# What commands are run
systemctl stop systemd-resolved
systemctl disable systemd-resolved
mv /etc/resolv.conf /etc/resolv.conf.bak
vim /etc/resolv.conf  (below)
reboot


# What files are edited \*Needs to have .bak for original comparison
/etc/systemd/resolved.conf
  dnsstublistener=no

/etc/dnsmasq.conf
  interface=bat0
  except-interface={{ ethernet }}
  #listen-address=
  expand-hosts
  domain=mesh.local
  dhcp-range=*default*

/etc/network/interface
  iface bat0 inet static
  # Must work with dnsmasq
    address 192.168.0.1
    network 255.255.255.0

/etc/resolv.conf
  nameserver 1.1.1.1


# What is use to troubleshoot
systemctl status dnsmasq
systemctl restart dnsmasq

needs to bind to port 53 (systemd resolvd connects to that)
 
tree (pkg)
tree (is the command)

verification: ss -ta
check for dns servers: dig
